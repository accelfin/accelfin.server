package com.accelfin.server;

import java.time.Instant;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class DefaultScheduler implements Scheduler {
    private ScheduledExecutorService _underlying;

    public DefaultScheduler() {
        _underlying = Executors.newScheduledThreadPool(1);
    }

    @Override
    public void schedule(Runnable runnable, long delayMilliseconds) {
        _underlying.schedule(runnable, delayMilliseconds, TimeUnit.MILLISECONDS);
    }

    @Override
    public Instant now() {
        return Instant.now();
    }
}
