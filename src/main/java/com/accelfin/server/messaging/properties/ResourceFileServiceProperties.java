package com.accelfin.server.messaging.properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;
import java.util.UUID;

public class ResourceFileServiceProperties implements ServiceProperties {
    private static Logger logger = LoggerFactory.getLogger(ResourceFileServiceProperties.class);
    private String _serviceType;
    private String _serviceId;
    private final HashMap<String, ResourceFileMqConnectionProperties> _connectionPropertiesByConnectionName = new HashMap<>();
    private final Properties _properties;

    public ResourceFileServiceProperties(String resourceFileName) {
        logger.debug("Loading properties");
        _properties = new Properties();
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        try {
            _properties.load(classLoader.getResourceAsStream(resourceFileName));
            _serviceType = _properties.getProperty("service.type");
            _serviceId = String.format("%s.%s", _serviceType, UUID.randomUUID().toString());
            logServiceDetails();
        } catch (IOException e) {
            logger.error("Error loading properties", e);
            throw new RuntimeException("Could not load properties");
        }
    }

    public ResourceFileMqConnectionProperties getMqConnectionProperties(String connectionId) {
        ResourceFileMqConnectionProperties connectionProperties = _connectionPropertiesByConnectionName.get(connectionId);
        if(connectionProperties == null) {
            connectionProperties = new ResourceFileMqConnectionProperties(_properties, connectionId);
            _connectionPropertiesByConnectionName.put(connectionId, connectionProperties);
        }
        return connectionProperties;
    }

    public String getServiceId() {
        return _serviceId;
    }

    public String getServiceType() {
        return _serviceType;
    }

    private void logServiceDetails(){
        logger.info("Service type:id => [{}:{}]", _serviceType, _serviceId);
    }

    public class ResourceFileMqConnectionProperties implements MqConnectionProperties {

        private Properties _properties;
        private String _connectionId;

        public ResourceFileMqConnectionProperties(Properties properties, String connectionId) {
            _properties = properties;
            _connectionId = connectionId;
        }

        public String getConnectionId() {
            return _connectionId;
        }

        public String getRabbitmqHost() {
            return _properties.getProperty(String.format("rabbitmq.%s.host", _connectionId));
        }

        public String getRabbitmqVHost() {
            return _properties.getProperty(String.format("rabbitmq.%s.vHost", _connectionId));
        }

        public String getRabbitmqUsername() {
            return _properties.getProperty(String.format("rabbitmq.%s.username", _connectionId));
        }

        public String getRabbitmqPassword() {
            return _properties.getProperty(String.format("rabbitmq.%s.password", _connectionId));
        }
    }
}