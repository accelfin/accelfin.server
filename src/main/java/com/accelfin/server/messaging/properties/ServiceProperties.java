package com.accelfin.server.messaging.properties;

public interface ServiceProperties {
    String getServiceType();
    String getServiceId();
    MqConnectionProperties getMqConnectionProperties(String connectionId);
}
