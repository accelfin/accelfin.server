package com.accelfin.server.messaging.properties;

public interface MqConnectionProperties {
    String getConnectionId();
    String getRabbitmqHost();
    String getRabbitmqVHost();
    String getRabbitmqUsername();
    String getRabbitmqPassword();
}
