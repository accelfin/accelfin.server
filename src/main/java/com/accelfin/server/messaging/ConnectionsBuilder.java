package com.accelfin.server.messaging;

import com.accelfin.server.Clock;
import com.accelfin.server.DefaultScheduler;
import com.accelfin.server.Scheduler;
import com.accelfin.server.ServiceModelBase;
import com.accelfin.server.messaging.config.ConnectionConfig;
import com.accelfin.server.messaging.gateways.DefaultMqGateway;
import com.accelfin.server.messaging.gateways.MqGateway;
import com.accelfin.server.messaging.gateways.SerializationService;
import com.accelfin.server.messaging.mappers.HeartbeatMapper;
import com.accelfin.server.messaging.mappers.ServiceStatusMapper;
import com.accelfin.server.messaging.properties.MqConnectionProperties;
import com.accelfin.server.messaging.properties.ResourceFileServiceProperties;
import com.accelfin.server.messaging.properties.ServiceProperties;
import com.accelfin.server.messaging.status.RemoteServiceMonitor;
import com.accelfin.server.messaging.status.StatusMonitor;
import com.esp.Router;
import com.esp.SubRouter;
import com.esp.functions.Action1;
import com.esp.functions.Func1;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Map;
import java.util.stream.Collectors;

public class ConnectionsBuilder {

    private Clock<Connection> _clock;
    private ArrayList<ConnectionBuilder> _connections = new ArrayList<>();
    private ServiceProperties _resourceFileServiceProperties;
    private Scheduler _scheduler;
    private Router<? extends ServiceModelBase> _applicationRouter;

    public static ConnectionsBuilder create() {
        return new ConnectionsBuilder();
    }

    public ConnectionsBuilder withProperties(String resourcePropertiesFile) {
        _resourceFileServiceProperties = new ResourceFileServiceProperties(resourcePropertiesFile);
        return this;
    }

    public ConnectionsBuilder withProperties(ServiceProperties serviceProperties) {
        _resourceFileServiceProperties = serviceProperties;
        return this;
    }

    public ConnectionsBuilder withClock(Clock<Connection> clock) {
        _clock = clock;
        return this;
    }

    public <TModel extends ServiceModelBase> ConnectionsBuilder withRouter(Router<TModel> applicationRouter) {
        _applicationRouter = applicationRouter;
        return this;
    }

    public ConnectionsBuilder withSchedule(Scheduler scheduler) {
        _scheduler = scheduler;
        return this;
    }

    public ConnectionsBuilder withConnection(String connectionKey, Action1<ConnectionBuilder> onConfigure) {
        ConnectionBuilder builder = new ConnectionBuilder(connectionKey);
        _connections.add(builder);
        onConfigure.call(builder);
        return this;
    }

    public Connections build() {
        Map<String, Connection> connections = _connections
                .stream()
                .map(cb -> {
                    cb.withRouter(_applicationRouter);
                    return cb.build();
                })
                .collect(Collectors.toMap(
                        Connection::getConnectionId,
                        (c) -> c)
                );
        return new Connections(
                _resourceFileServiceProperties.getServiceId(),
                _resourceFileServiceProperties.getServiceType(),
                connections
        );
    }

    public class ConnectionBuilder {
        private Router<Connection> _router;
        private String _connectionId;
        private MqGateway _mqGateway;

        public ConnectionBuilder(String connectionId) {
            _connectionId = connectionId;
        }

        <TOther extends ServiceModelBase> ConnectionBuilder withRouter(Router<TOther> applicationRouter) {
            class MessagingSubRouter extends SubRouter<TOther, Connection> {
                public MessagingSubRouter(Router<TOther> parent, Func1<TOther, Connection> subModelSelector) {
                    super(parent, subModelSelector);
                }
            }
            _router = new MessagingSubRouter(applicationRouter, m -> m.getConnections().getConnection(_connectionId));
            return this;
        }

        public ConnectionBuilder withMqGateway(MqGateway mqGateway) {
            _mqGateway = mqGateway;
            return this;
        }

        Connection build() {
            Scheduler scheduler = _scheduler == null
                    ? new DefaultScheduler()
                    : _scheduler;
            MqConnectionProperties mqConnectionProperties = _resourceFileServiceProperties.getMqConnectionProperties(_connectionId);
            ConnectionConfig connectionConfig = new ConnectionConfig(
                    _resourceFileServiceProperties.getServiceId(),
                    _resourceFileServiceProperties.getServiceType(),
                    mqConnectionProperties.getConnectionId(),
                    mqConnectionProperties.getRabbitmqHost(),
                    mqConnectionProperties.getRabbitmqVHost(),
                    mqConnectionProperties.getRabbitmqUsername(),
                    mqConnectionProperties.getRabbitmqPassword()
            );
            Logger logger= LoggerFactory.getLogger(String.format("%s-%s", Connection.class.getSimpleName(), connectionConfig.getConnectionId()));
            ServiceStatusMapper serviceStatusMapper = new ServiceStatusMapper(_clock);
            StatusMonitor statusMonitor = new StatusMonitor(
                    logger,
                    _router,
                    connectionConfig,
                    _clock,
                    serviceStatusMapper,
                    scheduler
            );
            MqGateway gateway = _mqGateway;
            if (gateway == null) {
                gateway = new DefaultMqGateway(
                        logger,
                        connectionConfig,
                        _router,
                        new SerializationService(logger),
                        _clock,
                        scheduler,
                        statusMonitor
                );
            }
            InFlightRequests inFlightRequests = new InFlightRequests(
                    connectionConfig.getConnectionId(),
                    logger,
                    gateway,
                    _router
            );
            RemoteServiceMonitor remoteServiceMonitor = new RemoteServiceMonitor(
                    logger,
                    _router,
                    connectionConfig.getConnectionId(),
                    _clock,
                    new HeartbeatMapper(serviceStatusMapper)
            );
            return new Connection(
                logger,
                gateway,
                _router,
                inFlightRequests,
                remoteServiceMonitor,
                statusMonitor,
                connectionConfig,
                _clock
            );
        }
    }
}
