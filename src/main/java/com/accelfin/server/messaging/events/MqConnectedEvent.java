package com.accelfin.server.messaging.events;

public class MqConnectedEvent implements ConnectionBoundEvent {
    private String _connectionId;

    public MqConnectedEvent(String connectionId) {
        _connectionId = connectionId;
    }

    @Override
    public String getConnectionId() {
        return _connectionId;
    }
}
