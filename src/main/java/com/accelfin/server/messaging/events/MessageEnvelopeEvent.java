package com.accelfin.server.messaging.events;

import com.accelfin.server.AuthenticationStatus;
import com.accelfin.server.messaging.dtos.MessageEnvelopeDto;

// esp event to process inbound MessageEnvelopeDto s
public class MessageEnvelopeEvent implements ConnectionBoundEvent {
    private String _connectionId;
    private AuthenticationStatus _authenticationStatus = AuthenticationStatus.Unauthenticated;
    private String _replyTo;

    private MessageEnvelopeDto _messageEnvelopeDto;

    public MessageEnvelopeEvent(
            String connectionId,
            String replyTo,
            MessageEnvelopeDto messageEnvelopeDto
    ) {
        _connectionId = connectionId;
        _replyTo = replyTo;
        _messageEnvelopeDto = messageEnvelopeDto;
    }

    public String getConnectionId() {
        return _connectionId;
    }

    public MessageEnvelopeDto getEnvelope() {
        return _messageEnvelopeDto;
    }

    public String getReplyTo() {
        return _replyTo;
    }

    /**
     * Gets the AuthenticationStatus of the current session that published this event
     *
     * @return
     */
    public AuthenticationStatus getAuthenticationStatus() {
        return _authenticationStatus;
    }

    /**
     * Sets the AuthenticationStatus of the current session that published this event
     *
     * This property should be managed by the consuming library.
     * Events deemed unauthenticated should be logged and dropped or an appropriate action taken.
     *
     * @return
     */
    public void setAuthenticationStatus(AuthenticationStatus authenticationStatus) {
        _authenticationStatus = authenticationStatus;
    }
}
