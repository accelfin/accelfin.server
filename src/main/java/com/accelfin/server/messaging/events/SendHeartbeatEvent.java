package com.accelfin.server.messaging.events;

public class SendHeartbeatEvent implements ConnectionBoundEvent {
    private static int _counter = 0;
    private String _connectionId;

    public SendHeartbeatEvent(String connectionId) {
        _connectionId = connectionId;
        _counter++;
    }

    public int getCounter() {
        return _counter;
    }

    @Override
    public String getConnectionId() {
        return _connectionId;
    }
}
