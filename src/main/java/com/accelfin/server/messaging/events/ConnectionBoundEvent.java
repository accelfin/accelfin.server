package com.accelfin.server.messaging.events;

public interface ConnectionBoundEvent {
    String getConnectionId();
}
