package com.accelfin.server.messaging.events;

import com.accelfin.server.messaging.config.operations.OperationConfig;

import java.time.Instant;

/**
 * A heartbeat from another service
 */
public class Heartbeat {

    private String _serviceType;
    private String _serviceId;
    private OperationConfig[] _operationConfigs;
    private Instant _timestamp;

    public Heartbeat(String serviceType, String serviceId, OperationConfig[] operationConfigs, Instant timestamp) {
        _serviceType = serviceType;
        _serviceId = serviceId;
        _operationConfigs = operationConfigs;
        _timestamp = timestamp;
    }

    public String getServiceType() {
        return _serviceType;
    }

    public String getServiceId() {
        return _serviceId;
    }

    public OperationConfig[] getOperationConfigs() {
        return _operationConfigs;
    }

    public Instant getTimestamp() {
        return _timestamp;
    }
}