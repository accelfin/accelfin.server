package com.accelfin.server.messaging.mappers;

import com.accelfin.server.messaging.config.operations.OperationConfig;
import com.accelfin.server.messaging.dtos.HeartbeatDto;
import com.accelfin.server.messaging.dtos.MessageEnvelopeDto;
import com.accelfin.server.messaging.events.Heartbeat;

import java.time.Instant;

public class HeartbeatMapper {

    private ServiceStatusMapper _serviceStatusMapper;

    public HeartbeatMapper(ServiceStatusMapper serviceStatusMapper) {
        _serviceStatusMapper = serviceStatusMapper;
    }

    public Heartbeat mapFromDto(MessageEnvelopeDto dto) {
        HeartbeatDto heartbeatDto = AnyDtoMapper.mapFromDto(dto.getPayload());
        return new Heartbeat(
                heartbeatDto.getServiceType(),
                heartbeatDto.getServiceId(),
                heartbeatDto.getOperationConfigDtosList().stream().map(_serviceStatusMapper::mapFromDto).toArray(OperationConfig[]::new),
                Instant.ofEpochMilli(heartbeatDto.getTimestamp().getSeconds())
        );
    }
}
