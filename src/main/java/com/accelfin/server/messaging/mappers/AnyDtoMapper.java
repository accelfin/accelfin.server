package com.accelfin.server.messaging.mappers;

import com.accelfin.server.messaging.dtos.AnyDto;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Message;
import com.google.protobuf.MessageLite;

public class AnyDtoMapper {

    public static AnyDto mapMessageToDto(Message payload) throws InvalidProtocolBufferException {
        // because we don't know the type of the payload we serialise it here along with the type info.
        // In theory the proto3 'Any' type should do this but i couldn't get it to work.
        // All the examples for Any appear to require you have the `class` when you call `unpack` on the `Any` message,
        // obviously I wouldn't have that here. I must doing something wrong with that.
        // Either way Any works like AnyDto in that it just contains a base46 encoded byte array of the payload,
        // which is a shame, means you looks transparency when using JSON.
        // Note we use the base 64 encoding so we can unwrap it on the client, it's ulgy but
        // it's only in this class and in the receiver on the client, then we have protobuf right through
        String canonicalName = payload.getClass().getCanonicalName();
        return AnyDto.newBuilder()
                .setCanonicalName(canonicalName)
                .setValue(payload.toByteString())
                .build();
    }

    public static <T extends Message> T mapFromDto(AnyDto dto)  {
        try {
            Message message = mapMessageFromDto(dto);
            return (T)message;
        } catch (Exception e) {
            throw new RuntimeException("Error deserialilsing heartbeat", e);
        }
    }

    public static Message mapMessageFromDto(AnyDto dto)  {
        try {
            Class c = Class.forName(dto.getCanonicalName());
            MessageLite defaultInstance = com.google.protobuf.Internal.getDefaultInstance(c);
            return (Message)defaultInstance
                    .getParserForType()
                    .parseFrom(dto.getValue());
        } catch (Exception e) {
            throw new RuntimeException("Error deserialilsing an AnyDto", e);
        }
    }
}
