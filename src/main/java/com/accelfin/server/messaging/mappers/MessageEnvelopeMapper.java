//package com.accelfin.server.messaging.mappers;
//
//import com.accelfin.server.SessionToken;
//import com.accelfin.server.messaging.MessageEnvelope;
//import com.accelfin.server.messaging.InboundMessage;
//
//public class MessageEnvelopeMapper {
//    static void mapBaseFields(InboundMessage event, MessageEnvelope envelope) {
//        event.setSessionToken(new SessionToken(envelope.getSessionId(), envelope.getConnectionId()));
//        event.setOperationName(envelope.getOperationName());
//        event.setCorrelationId(envelope.getCorrelationId());
//        event.setReplyTo(envelope.getReplyTo());
//        event.setTimestamp(envelope.getTimestamp());
//        event.setIsOperationTermination(envelope.getHasCompleted() == null ? false : envelope.getHasCompleted());
//    }
//}
