package com.accelfin.server.messaging.operations;

import com.accelfin.server.AuthenticationStatus;
import com.accelfin.server.Clock;
import com.accelfin.server.messaging.*;
import com.accelfin.server.messaging.config.ConnectionConfig;
import com.accelfin.server.messaging.config.operations.OperationConfig;
import com.accelfin.server.messaging.config.operations.OperationOwnership;
import com.accelfin.server.messaging.config.operations.OperationType;
import com.accelfin.server.messaging.InboundMessage;
import com.accelfin.server.messaging.events.MessageEnvelopeEvent;
import com.accelfin.server.messaging.gateways.InboundOutboundMessageMapper;
import com.esp.ObserveEvent;
import com.esp.Router;
import com.esp.reactive.Observable;
import com.esp.reactive.Subject;
import com.google.protobuf.Message;

// TODO All these need to add their configuration like the old code did
public class ServerRequestStreamOperationBase<TRequestDto extends Message, TResponseDto extends Message> extends ConnectionBoundEntity {
    private Subject<MessageEnvelopeEvent> _requestSubject = Subject.create();
    private Router<Connection> _router;
    private OutboundMessageHandler _outboundMessageHandler;
    private Clock<Connection> _clock;
    private final OperationConfig _operationConfig;

    public ServerRequestStreamOperationBase(
            Router<Connection> router,
            OutboundMessageHandler outboundMessageHandler,
            ConnectionConfig connectionConfig,
            String operationName,
            AuthenticationStatus requiredAuthenticationStatus,
            Clock<Connection> clock
    ) {
        super(connectionConfig.getConnectionId(), router);

        _router = router;
        _outboundMessageHandler = outboundMessageHandler;
        _clock = clock;
        _operationConfig = connectionConfig.getOrAddOperation(
                OperationOwnership.OwnedByThisService,
                OperationType.RequestStream,
                operationName,
                requiredAuthenticationStatus == AuthenticationStatus.Authenticated
        );
    }

    public boolean isAvailable() {
        return _operationConfig.getIsAvailable();
    }

    public void updateAvailability(boolean available) {
        _operationConfig.setIsAvailable(available);
    }

    protected OperationConfig getOperationConfig() {
        return _operationConfig;
    }

    protected OutboundMessageHandler getOutboundMessageHandler() {
        return _outboundMessageHandler;
    }

    public Observable<InboundMessage<TRequestDto>> requests() {
        return Observable.create(o ->
                _requestSubject
                        .map((MessageEnvelopeEvent envelopeEvent) -> InboundOutboundMessageMapper.<TRequestDto>mapInboundMessageFromDto(envelopeEvent, _clock.now()))
                        .observe(o)
        );
    }

    @ObserveEvent(eventClass = MessageEnvelopeEvent.class)
    public void onMessageEnvelopeReceived(MessageEnvelopeEvent event) {
        if (!isForThisConnection(event)) {
            return;
        }
        if (!event.getEnvelope().getOperationName().equals(_operationConfig.getOperationName())) {
            return;
        }
        _requestSubject.onNext(event);
    }
}
