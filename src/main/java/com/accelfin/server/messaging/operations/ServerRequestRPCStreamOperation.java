package com.accelfin.server.messaging.operations;

import com.accelfin.server.AuthenticationStatus;
import com.accelfin.server.Clock;
import com.accelfin.server.messaging.*;
import com.accelfin.server.messaging.config.ConnectionConfig;
import com.accelfin.server.messaging.InboundMessage;
import com.esp.Router;
import com.google.protobuf.Message;

public class ServerRequestRPCStreamOperation<TRequestDto extends Message, TResponseDto extends Message> extends ServerRequestStreamOperationBase<TRequestDto, TResponseDto> {

    public ServerRequestRPCStreamOperation(
            Router<Connection> router,
            OutboundMessageHandler outboundMessageHandler,
            ConnectionConfig connectionConfig,
            String operationName,
            AuthenticationStatus requiredAuthenticationStatus,
            Clock<Connection> clock
    ) {
        super(router, outboundMessageHandler, connectionConfig, operationName, requiredAuthenticationStatus, clock);
    }

    public void sendRpcResponse(
            InboundMessage<TRequestDto> initialRequest,
            TResponseDto responsePayload,
            boolean streamCompleted
    ) {
        getOutboundMessageHandler().handle(
                new OutboundMessage(
                        getOperationConfig().getOperationName(),
                        initialRequest.getCorrelationId(),
                        MessageDirection.Response,
                        getOperationConfig(),
                        responsePayload,
                        initialRequest.getReplyTo(),
                        null,
                        streamCompleted,
                        null,
                        initialRequest.getSessionToken().getSessionId()
                )
        );
    }
}
