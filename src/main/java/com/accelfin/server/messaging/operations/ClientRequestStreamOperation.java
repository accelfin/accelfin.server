package com.accelfin.server.messaging.operations;

import com.accelfin.server.Clock;
import com.accelfin.server.Item;
import com.accelfin.server.messaging.*;
import com.accelfin.server.messaging.config.ConnectionConfig;
import com.accelfin.server.messaging.config.operations.OperationConfig;
import com.accelfin.server.messaging.events.ClockTickEvent;
import com.accelfin.server.messaging.events.MessageEnvelopeEvent;
import com.accelfin.server.messaging.gateways.InboundOutboundMessageMapper;
import com.accelfin.server.messaging.status.RemoteServiceMonitor;
import com.esp.ObservationStage;
import com.esp.ObserveEvent;
import com.esp.Router;
import com.esp.disposables.CollectionDisposable;
import com.esp.disposables.Disposable;
import com.esp.reactive.Observable;
import com.esp.reactive.Subject;
import com.google.protobuf.Message;

import java.util.Optional;
import java.util.UUID;

public class ClientRequestStreamOperation<TRequestDto extends Message, TResponseDto extends Message> extends ConnectionBoundEntity {
    private Subject<MessageEnvelopeEvent> _responseSubject = Subject.create();
    private Router<Connection> _router;
    private OutboundMessageHandler _outboundMessageHandler;
    private RemoteServiceMonitor _remoteServiceMonitor;
    private ConnectionConfig _connectionConfig;
    private Clock<Connection> _clock;
    private String _serviceType;
    private String _operationName;
    private boolean _waitForConnection;

    public ClientRequestStreamOperation(
            Router<Connection> router,
            OutboundMessageHandler outboundMessageHandler,
            RemoteServiceMonitor remoteServiceMonitor,
            ConnectionConfig connectionConfig,
            Clock<Connection> clock,
            String serviceType,
            String operationName,
            boolean waitForConnection
    ) {
        super(connectionConfig.getConnectionId(), router);

        _router = router;
        _outboundMessageHandler = outboundMessageHandler;
        _remoteServiceMonitor = remoteServiceMonitor;
        _connectionConfig = connectionConfig;
        _clock = clock;
        _serviceType = serviceType;
        _operationName = operationName;
        _waitForConnection = waitForConnection;
    }

    public Observable<InboundMessage<TResponseDto>> sendRequest(TRequestDto request) {
        return Observable.create(o -> {
            String correlationId = UUID.randomUUID().toString();
            CollectionDisposable disposables = new CollectionDisposable();
            disposables.add(
                    _responseSubject
                            .where((MessageEnvelopeEvent envelopeEvent) -> envelopeEvent.getEnvelope().getCorrelationId().equals(correlationId))
                            .map((MessageEnvelopeEvent envelopeEvent) -> InboundOutboundMessageMapper.<TResponseDto>mapInboundMessageFromDto(envelopeEvent,  _clock.now()))
                            .observe((InboundMessage<TResponseDto> inboundMessage) -> {
                                o.onNext(inboundMessage);
                                if (inboundMessage.isCompleted()) {
                                    o.onCompleted();
                                }
                            })
            );
            Optional<OperationConfig> config = _remoteServiceMonitor.getOperationConfig(_serviceType, _operationName);
            if (config.isPresent()) {
                sendRequestMessage(correlationId, config.get(), request);
            } else if (_waitForConnection) {
                final Item<Long> lastCheckEpochMs = new Item<>(_clock.now().toEpochMilli());
                final Item<Disposable> waitForConnectionSubscription = new Item<>();
                waitForConnectionSubscription.setItem(_router
                        .getEventObservable(ClockTickEvent.class, ObservationStage.Committed)
                        .observe((e, c, m) -> {
                            if (_clock.now().toEpochMilli() > lastCheckEpochMs.getItem() + 1000) {
                                Optional<OperationConfig> config1 = _remoteServiceMonitor.getOperationConfig(_serviceType, _operationName);
                                if (config1.isPresent()) {
                                    waitForConnectionSubscription.getItem().dispose();
                                    sendRequestMessage(correlationId, config.get(), request);
                                } else {
                                    lastCheckEpochMs.setItem(_clock.now().toEpochMilli());
                                }
                            }
                        })
                );
                disposables.add(waitForConnectionSubscription.getItem());
            } else {
                throw new RuntimeException("TODO obs.onError()");
                // o.onError(/*TODO*/);
            }
            return disposables;
        });
    }

    @ObserveEvent(eventClass = MessageEnvelopeEvent.class)
    public void onMessageEnvelopeReceived(MessageEnvelopeEvent event) {
        if (!isForThisConnection(event)) {
            return;
        }
        if (!event.getEnvelope().getOperationName().equals(_operationName)) {
            return;
        }
        _responseSubject.onNext(event);
    }

    private void sendRequestMessage(String correlationId, OperationConfig operationConfig, Message payload) {
        _outboundMessageHandler.handle(
                new OutboundMessage(
                        operationConfig.getOperationName(),
                        correlationId,
                        MessageDirection.Request,
                        operationConfig,
                        payload,
                        null,
                        null,
                        null,
                        null,
                        _connectionConfig.getUserName()
                )
        );
    }
}

