package com.accelfin.server.messaging.operations;

import com.accelfin.server.messaging.OutboundMessageHandler;
import com.accelfin.server.messaging.InboundMessage;
import com.esp.reactive.Observable;
import com.google.protobuf.Message;

public class ClientStreamOperation<TResponseDto extends Message> {
    public ClientStreamOperation(
            OutboundMessageHandler outboundMessageHandler,
            String serviceType,
            String operationName,
            boolean waitForConnection
    ) {
    }

    public Observable<InboundMessage<TResponseDto>> responses() {
        return Observable.create(O -> {
            return () -> {};
        });
    }
}

