package com.accelfin.server.messaging.operations;

import com.accelfin.server.AuthenticationStatus;
import com.accelfin.server.Clock;
import com.accelfin.server.messaging.*;
import com.accelfin.server.messaging.config.ConnectionConfig;
import com.esp.Router;
import com.google.protobuf.Message;

public class ServerRequestStreamOperation<TRequestDto extends Message, TResponseDto extends Message> extends ServerRequestStreamOperationBase<TRequestDto, TResponseDto> {
    public ServerRequestStreamOperation(
            Router<Connection> router,
            OutboundMessageHandler outboundMessageHandler,
            ConnectionConfig connectionConfig,
            String operationName,
            AuthenticationStatus requiredAuthenticationStatus,
            Clock<Connection> clock
    ) {
        super(router, outboundMessageHandler, connectionConfig, operationName, requiredAuthenticationStatus, clock);
    }

    public void sendResponse(
            InboundMessage<TRequestDto> initialRequest,
            TResponseDto responsePayload,
            boolean streamCompleted,
            String... sessionTokens
    ) {
        getOutboundMessageHandler().handle(
                new OutboundMessage(
                        getOperationConfig().getOperationName(),
                        null,
                        MessageDirection.Response,
                        getOperationConfig(),
                        responsePayload,
                        null,
                        null,
                        streamCompleted,
                        null,
                        sessionTokens
                )
        );
    }

}
