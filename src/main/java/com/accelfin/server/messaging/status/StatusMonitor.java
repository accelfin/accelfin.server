package com.accelfin.server.messaging.status;

import com.accelfin.server.Clock;
import com.accelfin.server.Scheduler;
import com.accelfin.server.messaging.*;
import com.accelfin.server.messaging.config.ConnectionConfig;
import com.accelfin.server.messaging.config.operations.*;
import com.accelfin.server.messaging.dtos.HeartbeatDto;
import com.accelfin.server.messaging.events.MqConnectedEvent;
import com.accelfin.server.messaging.events.SendHeartbeatEvent;
import com.accelfin.server.messaging.mappers.ServiceStatusMapper;
import com.esp.EventContext;
import com.esp.ObserveEvent;
import com.esp.Router;
import org.slf4j.Logger;

import java.util.HashMap;

public class StatusMonitor extends ConnectionBoundEntity {

    public static final int HEARTBEAT_INTERVAL_MILLISECONDS = 5000;

    private Logger _logger;
    private Router<? extends Connection> _router;
    private ConnectionConfig _connectionConfig;
    private Clock _clock;
    private ServiceStatusMapper _serviceStatusMapper;
    private OperationConfig _heartbeatOperationConfig;
    private Scheduler _scheduler;
    private HashMap<String, MqNamingStrategy> _mqNamingStrategyByOperationName = new HashMap<>();

    public StatusMonitor(
            Logger logger,
            Router<Connection> router,
            ConnectionConfig connectionConfig,
            Clock clock,
            ServiceStatusMapper serviceStatusMapper,
            Scheduler scheduler
    ) {
        super(connectionConfig.getConnectionId(), router);
        _logger = logger;
        _router = router;
        _connectionConfig = connectionConfig;
        _clock = clock;
        _serviceStatusMapper = serviceStatusMapper;
        _heartbeatOperationConfig = new OperationConfig(
                OperationOwnership.OwnedByThisService,
                OperationType.Stream,
                OperationNames.Heartbeat,
                connectionConfig.getServiceType(),
                connectionConfig.getServiceId()
        );
        _scheduler = scheduler;
        for (OperationConfig operationConfig : connectionConfig.getOperations()) {
            _mqNamingStrategyByOperationName.put(operationConfig.getOperationName(), new DefaultOperationMqNamingStrategy(operationConfig));
        }
    }

    public void setOperationStatus(String operationName, boolean isAvailable){
        if (_connectionConfig.hasOperation(operationName)) {
            _connectionConfig.getOperationConfig(operationName).setIsAvailable(isAvailable);
        }
    }

    public boolean getIsOperationAvailable(String operationName) {
        if (_connectionConfig.hasOperation(operationName)) {
            return _connectionConfig.getOperationConfig(operationName).getIsAvailable();
        }
        return false;
    }

    @ObserveEvent(eventClass = MqConnectedEvent.class)
    public void onMqConnectedEvent(MqConnectedEvent event) {
        if (!isForThisConnection(event)) {
            return;
        }
        _logger.info("starting status heartbeating");
        setOperationStatus(OperationNames.Heartbeat, true);
        _scheduler.schedule(this::publishSendHeartbeatEvent, HEARTBEAT_INTERVAL_MILLISECONDS);
    }

    @ObserveEvent(eventClass = SendHeartbeatEvent.class)
    public void onSendHeartbeatEvent(SendHeartbeatEvent e, EventContext context, Connection model) {
        if (!isForThisConnection(e)) {
            return;
        }
        _logger.debug("Creating heartbeat getStreamOperation");
        if (e.getCounter() % 10 == 0) {
            _logger.trace("Sending heartbeat, not all get logged");
        }
        HeartbeatDto heartbeatDto = _serviceStatusMapper.mapToDto(
                _connectionConfig.getServiceType(),
                _connectionConfig.getServiceId(),
                _connectionConfig.getOperations(),
                _mqNamingStrategyByOperationName
        );
        model.getOutboundMessages().add(
                new OutboundMessage(
                        _heartbeatOperationConfig.getOperationName(),
                        null,
                        MessageDirection.Response,
                        _heartbeatOperationConfig,
                        heartbeatDto,
                        null,
                        null,
                        false,
                        HEARTBEAT_INTERVAL_MILLISECONDS
                )
        );
    }

    private void publishSendHeartbeatEvent() {
        if(isDisposed()) return;
        _router.publishEvent(new SendHeartbeatEvent(getConnectionId()));
        _scheduler.schedule(this::publishSendHeartbeatEvent, HEARTBEAT_INTERVAL_MILLISECONDS);
    }
}
