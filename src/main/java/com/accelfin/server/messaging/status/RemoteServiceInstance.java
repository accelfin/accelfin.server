package com.accelfin.server.messaging.status;

import com.accelfin.server.Clock;
import com.accelfin.server.messaging.config.operations.OperationConfig;
import org.slf4j.Logger;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public class RemoteServiceInstance {

    private Logger _logger;

    private Clock _clock;
    private String _serviceType;
    private String _serviceId;
    private Map<String, OperationConfig> _operationConfigsByOperationName;
    private Instant _lastUpdateTimestamp;
    private boolean _isConnected;
    private static final int DISCONNECT_MS = 10000;

    public RemoteServiceInstance(
            Clock clock,
            String serviceType,
            String serviceId,
            OperationConfig[] operationConfigs,
            Logger logger
    ) {
        _clock = clock;
        _serviceType = serviceType;
        _serviceId = serviceId;
        _operationConfigsByOperationName = Arrays
                .stream(operationConfigs)
                .collect(Collectors.toMap(OperationConfig::getOperationName, (p) -> p));
        _logger = logger;
        updateLastSeenTimestamp();
    }

    public String getServiceType() {
        return _serviceType;
    }

    public String getServiceId() {
        return _serviceId;
    }

    public OperationConfig getOperationConfig(String operationName) {
        return _operationConfigsByOperationName.get(operationName);
    }

    public void updateLastSeenTimestamp() {
        _lastUpdateTimestamp = _clock.now();
    }

    public void updateIsConnected() {
        boolean oldIsConnected = _isConnected;
        _isConnected = _lastUpdateTimestamp.plus(DISCONNECT_MS, ChronoUnit.MILLIS).isAfter(_clock.now());
        if(oldIsConnected != _isConnected) {
            if(oldIsConnected) {
                _logger.warn("Service [{}] id [{}] has gone offline", _serviceType, _serviceId);
            } else {
                _logger.info("Service [{}] id [{}] has come online", _serviceType, _serviceId);
            }

        }
    }

    public boolean getIsConnected() {
        return _isConnected;
    }
}
