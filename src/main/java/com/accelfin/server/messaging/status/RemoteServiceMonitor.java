package com.accelfin.server.messaging.status;

import com.accelfin.server.Clock;
import com.accelfin.server.messaging.Connection;
import com.accelfin.server.messaging.ConnectionBoundEntity;
import com.accelfin.server.messaging.config.operations.OperationConfig;
import com.accelfin.server.messaging.events.ClockTickEvent;
import com.accelfin.server.messaging.events.Heartbeat;
import com.accelfin.server.messaging.events.MessageEnvelopeEvent;
import com.accelfin.server.messaging.mappers.HeartbeatMapper;
import com.esp.ObservationStage;
import com.esp.ObserveEvent;
import com.esp.Router;
import org.slf4j.Logger;

import java.util.HashMap;
import java.util.Optional;

public class RemoteServiceMonitor extends ConnectionBoundEntity {

    private Logger _logger;

    private Router<Connection> _router;
    private Clock _clock;
    private HeartbeatMapper _heartbeatMapper;
    private HashMap<String, RemoteService> _remoteServiceByServiceType = new HashMap<>();
    private long _lastCheckEpochMs;

    public RemoteServiceMonitor(
            Logger logger,
            Router<Connection> router,
            String connectionId,
            Clock clock,
            HeartbeatMapper heartbeatMapper
    ) {
        super(connectionId, router);
        _logger = logger;
        _router = router;
        _clock = clock;
        _heartbeatMapper = heartbeatMapper;
    }

    @ObserveEvent(eventClass = MessageEnvelopeEvent.class)
    public void onHeartbeatEvent(MessageEnvelopeEvent event) {
        if (!isForThisConnection(event)) {
            return;
        }
        Heartbeat heartbeat = _heartbeatMapper.mapFromDto(event.getEnvelope());
        _logger.info("Processing heartbeat event [{}]. Config dto length [{}]", heartbeat.getTimestamp(), heartbeat.getOperationConfigs().length);
        RemoteService remoteService = _remoteServiceByServiceType.computeIfAbsent(heartbeat.getServiceType(), serviceType -> new RemoteService(_logger, _router, _clock));
        remoteService.onHeartbeatReceived(heartbeat);
    }

    @ObserveEvent(eventClass = ClockTickEvent.class, stage = ObservationStage.Committed)
    public void onTimerEvent(ClockTickEvent envelope) {
        if (_clock.now().toEpochMilli() > _lastCheckEpochMs + 1000) {
            _lastCheckEpochMs = _clock.now().toEpochMilli();
            _remoteServiceByServiceType.values().forEach(RemoteService::updateIsConnected);
        }
    }

    public Optional<OperationConfig> getOperationConfig(String serviceType, String operationName) {
        if (!_remoteServiceByServiceType.containsKey(serviceType)) {
            return Optional.empty();
        }
        RemoteService remoteService = _remoteServiceByServiceType.get(serviceType);
        if (!remoteService.hasAvailableInstance()) {
            return Optional.empty();
        }
        OperationConfig operationConfig = remoteService.getAvailableInstance().getOperationConfig(operationName);
        return Optional.of(operationConfig);
    }
}
