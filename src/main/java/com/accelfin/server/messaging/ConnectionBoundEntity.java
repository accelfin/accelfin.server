package com.accelfin.server.messaging;

import com.accelfin.server.messaging.events.ConnectionBoundEvent;
import com.esp.Router;
import com.esp.disposables.DisposableBase;

public abstract class ConnectionBoundEntity extends DisposableBase {

    private String _connectionId;
    private Router<Connection> _router;

    public ConnectionBoundEntity(String connectionId, Router<Connection> router) {
        _connectionId = connectionId;
        _router = router;
    }

    public void observeEvents() {
        addDisposable(_router.observeEventsOn(this));
    }

    public String getConnectionId() {
        return _connectionId;
    }

    protected boolean isForThisConnection(ConnectionBoundEvent event) {
        return event.getConnectionId().equals(_connectionId);
    }
}
