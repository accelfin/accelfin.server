package com.accelfin.server.messaging;

import com.accelfin.server.AuthenticationStatus;
import com.accelfin.server.Clock;
import com.accelfin.server.messaging.config.ConnectionConfig;
import com.accelfin.server.messaging.gateways.MqGateway;
import com.accelfin.server.messaging.operations.*;
import com.accelfin.server.messaging.status.RemoteServiceMonitor;
import com.accelfin.server.messaging.status.StatusMonitor;
import com.esp.Router;
import com.google.protobuf.Message;
import org.slf4j.Logger;

import java.util.ArrayList;

public class Connection extends ConnectionBoundEntity {

    private Logger _logger;
    private MqGateway _mqGateway;
    private Router<Connection> _router;
    private InFlightRequests _inFlightRequests;
    private RemoteServiceMonitor _remoteServiceMonitor;
    private StatusMonitor _statusMonitor;
    private ConnectionConfig _connectionConfig;
    private Clock<Connection> _clock;
    private ArrayList<OutboundMessage> _outboundMessages = new ArrayList<>();
    private OutboundMessageHandler _outboundMessageHandler;

    public Connection(
            Logger logger,
            MqGateway mqGateway,
            Router<Connection> router,
            InFlightRequests inFlightRequests,
            RemoteServiceMonitor remoteServiceMonitor,
            StatusMonitor statusMonitor,
            ConnectionConfig connectionConfig,
            Clock<Connection> clock
    ) {
        super(connectionConfig.getConnectionId(), router);

        _logger = logger;
        _mqGateway = mqGateway;
        _router = router;
        _inFlightRequests = inFlightRequests;
        _remoteServiceMonitor = remoteServiceMonitor;
        _statusMonitor = statusMonitor;
        _connectionConfig = connectionConfig;
        _clock = clock;
        _outboundMessageHandler = outboundMessage -> { _outboundMessages.add(outboundMessage); };
    }

    public void preProcess() {
        _outboundMessages.clear();
    }

    public ArrayList<OutboundMessage> getOutboundMessages() {
        return _outboundMessages;
    }

    @Override
    public void observeEvents() {
        super.observeEvents();
        _inFlightRequests.observeEvents();
        _remoteServiceMonitor.observeEvents();
        _statusMonitor.observeEvents();
        _mqGateway.observeEvents();
    }

    public void connect() {
        try {
            _logger.debug("connecting to mq");
            _mqGateway.connect();
        } catch (Exception e) {
            _logger.error("Error connecting to mq", e);
        }
    }

    public StatusMonitor getStatusMonitor() {
        return _statusMonitor;
    }

    public <TRequestDto extends Message, TResponseDto extends Message> ClientRequestStreamOperation<TRequestDto, TResponseDto> getRequestStreamOperation(
            String serviceType,
            String operationName,
            boolean waitForConnection
    ) {
        ClientRequestStreamOperation<TRequestDto, TResponseDto> operation = new ClientRequestStreamOperation<>(
                _router,
                _outboundMessageHandler,
                _remoteServiceMonitor,
                _connectionConfig,
                _clock,
                serviceType,
                operationName,
                waitForConnection
        );
        operation.observeEvents();
        return operation;
    }

    public <TResponseDto extends Message> ClientStreamOperation<TResponseDto> getStreamOperation(
            String serviceType,
            String operationName,
            boolean waitForConnection
    ) {
        ClientStreamOperation<TResponseDto> operation = new ClientStreamOperation<>(
                _outboundMessageHandler,
                serviceType,
                operationName,
                waitForConnection
        );
        return operation;
    }

    public <TRequestDto extends Message, TResponseDto extends Message> ServerRequestStreamOperation<TRequestDto, TResponseDto> exposeRequestStreamOperation(
            String operationName,
            AuthenticationStatus requiredAuthenticationStatus
    ) {
        ServerRequestStreamOperation<TRequestDto, TResponseDto> operation = new ServerRequestStreamOperation<>(
                _router,
                _outboundMessageHandler,
                _connectionConfig,
                operationName,
                requiredAuthenticationStatus,
                _clock
        );
        operation.observeEvents();
        return operation;
    }

    public <TRequestDto extends Message, TResponseDto extends Message> ServerRequestRPCStreamOperation<TRequestDto, TResponseDto> exposeRPCOperation(
            String operationName,
            AuthenticationStatus requiredAuthenticationStatus
    ) {
        ServerRequestRPCStreamOperation<TRequestDto, TResponseDto> operation = new ServerRequestRPCStreamOperation<>(
                _router,
                _outboundMessageHandler,
                _connectionConfig,
                operationName,
                requiredAuthenticationStatus,
                _clock
        );
        operation.observeEvents();
        return operation;
    }

    public <TResponseDto extends Message> ServerStreamOperation<TResponseDto> exposeStreamOperation(
            String operationName
    ) {
        return new ServerStreamOperation<>(
                _outboundMessageHandler,
                _connectionConfig,
                operationName
        );
    }
}
