package com.accelfin.server.messaging;

import java.util.Map;

public class Connections {
    private String _serviceId;
    private String _serviceType;
    private Map<String, Connection> _connectionsByKey;

    public Connections(String serviceId, String serviceType, Map<String, Connection> connectionsByKey) {
        _serviceId = serviceId;
        _serviceType = serviceType;
        _connectionsByKey = connectionsByKey;
    }

    public String getServiceId() {
        return _serviceId;
    }

    public String getServiceType() {
        return _serviceType;
    }

    public Connection getConnection(String key) {
        return _connectionsByKey.get(key);
    }

    public void observeEvents() {
        _connectionsByKey.values().forEach(Connection::observeEvents);
    }

    public void preProcess() {
        _connectionsByKey.values().forEach(Connection::preProcess);
    }

    public void connectAll() {
        _connectionsByKey.values().forEach(Connection::connect);
    }
}