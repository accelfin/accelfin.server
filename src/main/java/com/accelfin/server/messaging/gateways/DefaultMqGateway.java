package com.accelfin.server.messaging.gateways;

import com.accelfin.server.Clock;
import com.accelfin.server.Scheduler;
import com.accelfin.server.messaging.*;
import com.accelfin.server.messaging.Connection;
import com.accelfin.server.messaging.config.ExchangeConst;
import com.accelfin.server.messaging.config.ConnectionConfig;
import com.accelfin.server.messaging.config.operations.*;
import com.accelfin.server.messaging.dtos.MessageEnvelopeDto;
import com.accelfin.server.messaging.events.MessageEnvelopeEvent;
import com.accelfin.server.messaging.mappers.HeartbeatMapper;
import com.accelfin.server.messaging.events.MqConnectedEvent;
import com.accelfin.server.messaging.events.ReconnectMqEvent;
import com.accelfin.server.messaging.mappers.ServiceStatusMapper;
import com.accelfin.server.messaging.status.StatusMonitor;
import com.esp.ObserveEvent;
import com.esp.Router;
import com.esp.disposables.DisposableBase;
import com.rabbitmq.client.*;
import org.slf4j.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class DefaultMqGateway extends DisposableBase implements MqGateway {
    private Logger _logger;
    private ConnectionConfig _configuration;
    private Router<Connection> _router;
    private SerializationService _serializationService;
    private StatusMonitor _statusMonitor;
    private Clock _clock;
    private Scheduler _scheduler;
    private Channel _channel;
    private HashMap<String, MqNamingStrategy> _mqNamingStrategyByOperationName = new HashMap<>();
    private String _rpcResponseQueueName;

    public DefaultMqGateway(
            Logger logger,
            ConnectionConfig configuration,
            Router<Connection> router,
            SerializationService serializationService,
            Clock clock,
            Scheduler scheduler,
            StatusMonitor statusMonitor
    ) {
        _logger = logger;
        _configuration = configuration;
        _router = router;
        _serializationService = serializationService;
        _clock = clock;
        _scheduler = scheduler;
        _statusMonitor = statusMonitor;
        for (OperationConfig operationConfig : configuration.getOperations()) {
            _mqNamingStrategyByOperationName.put(
                    operationConfig.getOperationName(),
                    new DefaultOperationMqNamingStrategy(operationConfig)
            );
        }
        // TODO lift heartbeat to it's own 'built-in' exposed operation
        _mqNamingStrategyByOperationName.put(OperationNames.Heartbeat, new HeartbeatMqNamingStrategy(_configuration.getServiceType(), _configuration.getServiceId()));
        _rpcResponseQueueName = String.format("%s.rpc-response.%s",
                _configuration.getServiceType(),
                _configuration.getServiceId()
        );
        _logger.info("Serialisation service will be [{}]", _serializationService.getClass().getCanonicalName());
    }

    public void observeEvents() {
        addDisposable(_router.observeEventsOn(this));
    }

    @Override
    public void connect() throws Exception {
        _logger.debug("Connecting");
        wireUpMessageDispatch();
        createConnection();
    }

    @ObserveEvent(eventClass = ReconnectMqEvent.class)
    public void createConnection() {
        // if the cluster isn't yet up this throws (even with auto recovery)
        try {
            _logger.debug("Connecting with config {}", _configuration);
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost(_configuration.getHost());
            factory.setVirtualHost(_configuration.getVirtualHost());
            factory.setUsername(_configuration.getUserName());
            factory.setPassword(_configuration.getPassword());
            factory.setAutomaticRecoveryEnabled(true);
            factory.setTopologyRecoveryEnabled(true);
            factory.setExceptionHandler(new MqLoggingExceptionHandler(_logger));
            com.rabbitmq.client.Connection underlyingConnection = factory.newConnection();
            underlyingConnection.addShutdownListener(cause -> {
                _logger.warn("Rabbit shutdown, will schedule retry", cause);
                _scheduleReconnect();
            });
            // according to MQ docos we should just assume the connection is open now:
            // http://www.rabbitmq.com/api-guide.html#shutdown-atomicity
            // "we should normally ignore such checking (of channel.isOpen()), and simply attempt the action desired."
            _channel = underlyingConnection.createChannel();

            declareExchanges();
            declareRpcReplyToQueue();
            wireUpStatusObservation();
            wireUpOperationEndpoints();
            _router.publishEvent(new MqConnectedEvent(_configuration.getConnectionId()));
        } catch (Exception ex) {
            // it appears that if the cluster isn't there yet, or perhaps is there but not configured
            // rabbit blows up on the call to factory.newConnection() above.
            // given this we backoff for (a reasonable) 5 seconds, in normal operation we'll be relying
            // on the auto recovery stuff above.
            _logger.error("Exception connecting to MQ, will schedule retry", ex);
            _scheduleReconnect();
        }
    }

    private void _scheduleReconnect() {
        _logger.debug("Scheduling connect to MQ, will schedule retry");
        _scheduler.schedule(
                () -> {

                    _logger.debug("Publishing reconnect event");
                    _router.publishEvent(ReconnectMqEvent.Instance);
                },
                10000
        );
    }

    private void declareRpcReplyToQueue() throws Exception {
        _logger.debug("[NetworkBind] Wiring up rpc queue");
        _logger.debug("Declaring queue [{}]", _rpcResponseQueueName);
        // don't think these queues need to be exclusive, there are issues during reconnects when they are, in any case the queue names are unique to the service
        // http://stackoverflow.com/a/27985276/36146
        AMQP.Queue.DeclareOk rpcQueue = _channel.queueDeclare(_rpcResponseQueueName, false, false, true, null);
        _rpcResponseQueueName = rpcQueue.getQueue();
        _logger.debug("Binding rpc queue [{}] to exchange [{}]",
                _rpcResponseQueueName,
                ExchangeConst.RESPONSE_EXCHANGE_NAME
        );
        _channel.queueBind(rpcQueue.getQueue(), ExchangeConst.RESPONSE_EXCHANGE_NAME, _rpcResponseQueueName);
        wireUpQueueConsumer(
                _rpcResponseQueueName,
                String.format("rpc-response-%s", _configuration.getServiceId()));
    }

    private void declareExchanges() throws Exception {
        // declare or redeclare the well-known exchange names
        _logger.debug("Creating request direct exchange [{}]", ExchangeConst.REQUEST_EXCHANGE_NAME);
        _channel.exchangeDeclare(ExchangeConst.REQUEST_EXCHANGE_NAME, "direct");
        _logger.debug("Creating response direct exchange [{}]", ExchangeConst.RESPONSE_EXCHANGE_NAME);
        _channel.exchangeDeclare(ExchangeConst.RESPONSE_EXCHANGE_NAME, "direct");
    }

    private void wireUpStatusObservation() throws Exception {
        OperationConfig heartbeatConfig = new OperationConfig(
                OperationOwnership.OwnedByRemoteService,
                OperationType.Stream,
                OperationNames.Heartbeat,
                _configuration.getServiceType(),
                _configuration.getServiceId()
        );
        wireUpOperation(heartbeatConfig);
    }

    private void wireUpOperationEndpoints() throws Exception {
        for (OperationConfig operationConfig : _configuration.<OperationConfig>getOperations()) {
            boolean shouldWireIpMessageListener =
                    // for local operations we'll only want to listen to incoming requests for these operations, we'll push responses on the outgoing message path
                    (operationConfig.getOperationOwnership() == OperationOwnership.OwnedByThisService && (operationConfig.getOperationType() == OperationType.RequestStream || operationConfig.getOperationType() == OperationType.RPC)) ||
                            // for remote operations we'll want to listen for all incoming messages
                            (operationConfig.getOperationOwnership() == OperationOwnership.OwnedByRemoteService);
            if (shouldWireIpMessageListener) {
                wireUpOperation(operationConfig);
            } else {
                _logger.debug("[NetworkBind] Ignoring operation [{}:{}:{}]. It's not intended for incoming messages", operationConfig.getOperationName(), operationConfig.getOperationOwnership(), operationConfig.getOperationType());
            }
        }
    }

    private void wireUpOperation(OperationConfig operationConfig) throws Exception {
        MqNamingStrategy namingStrategy = _mqNamingStrategyByOperationName.get(operationConfig.getOperationName());
        _logger.debug("[NetworkBind] Wiring up operation [{}:{}]:{}", operationConfig.getOperationName(), operationConfig.getOperationOwnership(), operationConfig.getOperationType());
        _logger.debug("Declaring queue [{}]", namingStrategy.getQueueName());
        // don't think these queues need to be exclusive, there are issues during reconnects when they are, in any case the queue names are unique to the service
        // http://stackoverflow.com/a/27985276/36146
        AMQP.Queue.DeclareOk requestQueue = _channel.queueDeclare(namingStrategy.getQueueName(), false, false, true, null);
        // if the operation is local, i.e. to be owned and procesesd by this service instance ,
        // web bind the operations queue to the request exchange
        String exchange = operationConfig.getOperationOwnership() == OperationOwnership.OwnedByThisService
                ? ExchangeConst.REQUEST_EXCHANGE_NAME
                : ExchangeConst.RESPONSE_EXCHANGE_NAME;
        String routingKey = operationConfig.getOperationOwnership() == OperationOwnership.OwnedByThisService
                ? namingStrategy.getRequestRoutingKey()
                : namingStrategy.getResponseRoutingKey();
        _logger.debug("Binding queue [{}] to exchange [{}] using routing key [{}]",
                namingStrategy.getQueueName(),
                exchange,
                routingKey
        );
        _channel.queueBind(requestQueue.getQueue(), exchange, routingKey);
        wireUpQueueConsumer(
                requestQueue.getQueue(),
                String.format("%s.%s", operationConfig.getOperationName(), _configuration.getServiceId())
        );
    }

    private void wireUpQueueConsumer(String queue, String consumerTag) throws Exception {
        Consumer consumer = new DefaultConsumer(_channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope mqEnvelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                try {
                    MessageEnvelopeDto envelope = _serializationService.deserializeFromB64Bytes(body);
                    String replyTo = properties.getReplyTo();
                    if (envelope.getSenderInstanceId() != null && envelope.getSenderInstanceId().equals(_configuration.getServiceId())) {
                        // For some operations, such as heartbeating we receive the messages we sent.
                        // we drop them here. Heartbeating should be the only case where this is true.
                        // all other operations are not broadcast with such a wide key.
                        // TODO [optimisation refactor] :
                        return;
                    }
                    _logger.debug("Incoming message for operation [{}].\n[{}]",
                            envelope.getOperationName(),
                            envelope.toString()
                    );

                    // this isn't ideal: if the incomming message isn't in our configuration we assume it's a response and push it into the system like others.
                    // As a nice to hae we could add a DirectionType into the envelope and error here if that's wrong.
                    // But at the end of the day that could be fudged.
                    // TODO [optimisation refactor] : check in-flight request and respond with error or log and drop if it's an unknown message
                    // At the moment it'll get published and get ignored as there won't be anything listening to it.
                    boolean operationOwnedByThisService = _configuration.hasOperation(envelope.getOperationName());
                    boolean operationAvailable = _statusMonitor.getIsOperationAvailable(envelope.getOperationName());
                    if (operationOwnedByThisService && !operationAvailable) {
                        // if the operation is down we just immediately respond with an error
                        _logger.warn("Request received but operation {} is down. Sending error response", envelope.getOperationName());
                        OperationConfig operationConfig = _configuration.getOperationConfig(envelope.getOperationName());
                        OutboundMessage errorResponse = new OutboundMessage(
                                envelope.getOperationName(),
                                envelope.getCorrelationId(),
                                MessageDirection.Response,
                                operationConfig,
                                null,
                                replyTo,
                                "Operation not available",
                                true,
                                null,
                                envelope.getSenderSessionId()
                        );
                        dispatchMessage(errorResponse);
                    } else {
                        // this must be push to the system via an event so it can be journaled like any other event that affects state
                        _router.publishEvent(
                                new MessageEnvelopeEvent(
                                        _configuration.getConnectionId(),
                                        replyTo,
                                        envelope
                                )
                        );
                    }
                } catch (Exception e) {
                    _logger.error("Error processing message", e);
                }
            }
        };
        _channel.basicConsume(queue, true, consumerTag, consumer);
    }

    private void wireUpMessageDispatch() {
        addDisposable(_router.getModelObservable().observe(model -> {
            ArrayList<OutboundMessage> messages = model.getOutboundMessages();
            for (OutboundMessage message : messages) {
                dispatchMessage(message);
            }
        }));
    }

    private void dispatchMessage(OutboundMessage message) {
        try {
            MessageEnvelopeDto envelope = InboundOutboundMessageMapper.mapOutboundMessageToDto(
                    message,
                    _clock.now(),
                    _configuration.getServiceId()
            );
            _logger.debug("Outgoing message for operation [{}].\n[{}]",
                    envelope.getOperationName(),
                    envelope.toString()
            );
            byte[] payload = _serializationService.serializeToB64Bytes(envelope);

            OperationConfig operationConfig = message.getOperationConfig();
            if (message.getMessageDirection() == MessageDirection.Request) {
                if (operationConfig.getOperationType() == OperationType.RequestStream) {
                    throw new RuntimeException("Not supported");
                    // dispatchStreamRequest(message, operationConfig, payload);
                } else if (operationConfig.getOperationType() == OperationType.RPC) {
                    dispatchRpcRequest(message, operationConfig, payload);
                } else {
                    _logger.warn("Unsupported operation type {}", operationConfig.getOperationType());
                }
            } else if (message.getMessageDirection() == MessageDirection.Response) {
                if (message.getOperationName().equals(OperationNames.Heartbeat)) {
                    dispatchHeartbeatResponse(payload, message.getTtl());
                } else {
                    if (operationConfig.getOperationType() == OperationType.Stream || operationConfig.getOperationType() == OperationType.RequestStream) {
                        dispatchStreamResponse(message, operationConfig, payload);
                    } else if (operationConfig.getOperationType() == OperationType.RPC) {
                        dispatchRpcResponse(message, operationConfig, payload);
                    } else {
                        _logger.warn("Unsupported operation type {}", operationConfig.getOperationType());
                    }
                }
            } else {
                _logger.warn("Unsupported message direction {}", message.getMessageDirection());
            }
        } catch (Exception e) {
            _logger.error("Error sending message", e);
        }
    }
//    private void dispatchStreamRequest(OutboundMessage message, OperationConfig operationConfig, String payloadJson) {
//        try {
//            MqNamingStrategy namingStrategy = _mqNamingStrategyByOperationName.get(operationConfig.getOperationName());
//            String routingKey = namingStrategy.getRequestRoutingKey();
//            logDispatch(
//                    operationConfig.getOperationName(),
//                    ExchangeConst.REQUEST_EXCHANGE_NAME,
//                    routingKey,
//                    payloadJson
//            );
//            _channel.basicPublish(
//                    ExchangeConst.REQUEST_EXCHANGE_NAME,
//                    routingKey,
//                    null,
//                    payloadJson.getBytes()
//            );
//        } catch (Exception e) {
//            _logger.error("Error sending message", e);
//        }
//    }

    private void dispatchStreamResponse(OutboundMessage message, OperationConfig operationConfig, byte[] payload) {
        try {
            MqNamingStrategy namingStrategy = _mqNamingStrategyByOperationName.get(operationConfig.getOperationName());
            if (message.getHasSessionIds()) {
                for (String sessionId : message.getSessionIds()) {
                    String routingKey = namingStrategy.getResponseRoutingKey(sessionId);
                    logDispatch(
                            operationConfig.getOperationName(),
                            ExchangeConst.RESPONSE_EXCHANGE_NAME,
                            routingKey
                    );
                    _channel.basicPublish(
                            ExchangeConst.RESPONSE_EXCHANGE_NAME,
                            routingKey,
                            null,
                            payload
                    );
                }
            } else {
                String routingKey = namingStrategy.getResponseRoutingKey();
                logDispatch(
                        operationConfig.getOperationName(),
                        ExchangeConst.RESPONSE_EXCHANGE_NAME,
                        routingKey
                );
                // note that the doco uses bytes as the payloads
                // https://www.rabbitmq.com/api-guide.html
                // it appears that the receiver gets a proper string representation
                _channel.basicPublish(
                        ExchangeConst.RESPONSE_EXCHANGE_NAME,
                        routingKey,
                        null,
                        payload
                );
            }
        } catch (Exception e) {
            _logger.error("Error sending message", e);
        }
    }

    private void dispatchRpcRequest(OutboundMessage message, OperationConfig operationConfig, byte[] payload) throws IOException {
        MqNamingStrategy namingStrategy = new DefaultOperationMqNamingStrategy(operationConfig);
        logDispatch(
                operationConfig.getOperationName(),
                ExchangeConst.REQUEST_EXCHANGE_NAME,
                namingStrategy.getRequestRoutingKey()
        );
        AMQP.BasicProperties properties = new AMQP.BasicProperties
                .Builder()
                .replyTo(_rpcResponseQueueName)
                // .correlationId(message.getCorrelationId())
                .build();
        // fx.getPriceTicks-request.fx.665983fc-8068-43ca-8b33-7f206e2403d0

        // note that the doco uses bytes as the payloads
        // https://www.rabbitmq.com/api-guide.html
        // it appears that the receiver gets a proper string representation
        _channel.basicPublish(
                ExchangeConst.REQUEST_EXCHANGE_NAME,
                namingStrategy.getRequestRoutingKey(),
                properties,
                payload
        );
    }


    private void dispatchRpcResponse(OutboundMessage message, OperationConfig operationConfig, byte[] payload) throws IOException {
        String routingKey = message.getReplyTo();
        logDispatch(
                operationConfig.getOperationName(),
                "N/A",
                routingKey
        );
        // note that the doco uses bytes as the payloads
        // https://www.rabbitmq.com/api-guide.html
        // it appears that the receiver gets a proper string representation
        _channel.basicPublish(
                "",// no exchange
                routingKey,
                null,
                payload
        );
    }

    private void dispatchHeartbeatResponse(byte[] payload, Integer ttl) {
        // note we don't use operations routing. heartbeat is an edge case
        MqNamingStrategy namingStrategy = _mqNamingStrategyByOperationName.get(OperationNames.Heartbeat);
        logDispatch(
                OperationNames.Heartbeat,
                ExchangeConst.RESPONSE_EXCHANGE_NAME,
                namingStrategy.getResponseRoutingKey()
        );
        try {
            AMQP.BasicProperties properties = new AMQP.BasicProperties
                    .Builder()
                    .expiration(Integer.toString(ttl))
                    .build();
            // note that the doco uses bytes as the payloads
            // https://www.rabbitmq.com/api-guide.html
            // it appears that the receiver gets a proper string representation
            _channel.basicPublish(
                    ExchangeConst.RESPONSE_EXCHANGE_NAME,
                    namingStrategy.getResponseRoutingKey(),
                    properties,
                    payload
            );
        } catch (IOException e) {
            _logger.error("Error sending heartbeat message", e);
        }
    }

    // TODO is there a hook in rabbit so I can delete this method?
    protected void logDispatch(String operationName, String exchange, String routingKey) {
        _logger.debug(
                "Dispatching operation [{}] to exchange [{}] with key [{}]",
                operationName,
                exchange,
                routingKey
        );
    }
}
