package com.accelfin.server.messaging.gateways;

import com.esp.disposables.Disposable;

public interface MqGateway extends Disposable {
    void connect() throws Exception;
    void observeEvents();
}
