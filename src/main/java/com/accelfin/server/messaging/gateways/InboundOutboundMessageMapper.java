package com.accelfin.server.messaging.gateways;

import com.accelfin.server.SessionToken;
import com.accelfin.server.messaging.InboundMessage;
import com.accelfin.server.messaging.OutboundMessage;
import com.accelfin.server.messaging.dtos.MessageEnvelopeDto;
import com.accelfin.server.messaging.dtos.TimestampDto;
import com.accelfin.server.messaging.events.MessageEnvelopeEvent;
import com.accelfin.server.messaging.mappers.AnyDtoMapper;
import com.accelfin.server.messaging.mappers.SimpleTypesMapper;
import com.google.protobuf.Message;

import java.time.Instant;

public class InboundOutboundMessageMapper {
    public static MessageEnvelopeDto mapOutboundMessageToDto(
            OutboundMessage message,
            Instant now,
            String senderId
    ) throws Exception {
        return mapOutboundMessageToDto(message, now, senderId, null);
    }

    public static MessageEnvelopeDto mapOutboundMessageToDto(
            OutboundMessage message,
            Instant now,
            String senderId,
            String sessionId
    ) throws Exception {
        MessageEnvelopeDto.Builder builder = MessageEnvelopeDto.newBuilder();
        builder.setOperationName(message.getOperationName());
        builder.setTimestamp(TimestampDto.newBuilder().setSeconds(now.getEpochSecond()));
        if(message.getPayload() != null) builder.setPayload(AnyDtoMapper.mapMessageToDto(message.getPayload()));
        if(message.getHasCompleted() != null) builder.setHasCompleted(message.getHasCompleted()); // can be null for outgoing request
        if(message.getError() != null) builder.setError(message.getError());
        if(message.getCorrelationId() != null) builder.setCorrelationId(message.getCorrelationId());
        if(senderId != null) builder.setSenderInstanceId(senderId);
        if(sessionId != null) builder.setSenderSessionId(senderId);
        return builder.build();
    }

    public static <TResponseDto extends Message> InboundMessage<TResponseDto> mapInboundMessageFromDto(
            MessageEnvelopeEvent event,
            Instant now
    ) {
        MessageEnvelopeDto envelopeDto = event.getEnvelope();
        TResponseDto responseDto = AnyDtoMapper.mapFromDto(envelopeDto.getPayload());
        return new InboundMessage<>(
                envelopeDto.getCorrelationId(),
                envelopeDto.getOperationName(),
                new SessionToken(envelopeDto.getSenderSessionId(), event.getConnectionId()),
                event.getAuthenticationStatus(),
                event.getReplyTo(),
                envelopeDto.getHasCompleted(),
                SimpleTypesMapper.mapCompletionReasonFromDto(envelopeDto.getCompletionReason()),
                now,
                responseDto
        );
    }
}
