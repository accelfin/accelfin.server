package com.accelfin.server.messaging.config.operations;

import com.accelfin.server.messaging.OperationNames;

public class HeartbeatMqNamingStrategy extends DefaultOperationMqNamingStrategy {

    private String _serviceId;
    private String _serviceType;

    public HeartbeatMqNamingStrategy(String serviceType, String serviceId) {
        super(null);
        _serviceType = serviceType;
        _serviceId = serviceId;
    }

    @Override
    public String getQueueName() {
        return String.format("%s.%s-queue.%s",
                _serviceType,
                OperationNames.Heartbeat,
                _serviceId
        );
    }

    @Override
    public String getRequestRoutingKey() {
        throw new RuntimeException("Not supported");
    }

    @Override
    public String getResponseRoutingKey() {
        return "status.heartbeat";
    }

    @Override
    public String getResponseRoutingKey(String sessionId) {
        throw new RuntimeException("Not supported");
    }

}
