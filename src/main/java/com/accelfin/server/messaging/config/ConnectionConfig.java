package com.accelfin.server.messaging.config;

import com.accelfin.server.messaging.OperationNames;
import com.accelfin.server.messaging.config.operations.OperationConfig;
import com.accelfin.server.messaging.config.operations.OperationOwnership;
import com.accelfin.server.messaging.config.operations.OperationType;

import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

public class ConnectionConfig {
    private String _connectionId;
    private String _host;
    private String _virtualHost;
    private String _userName;
    private String _password;

    private Map<String, OperationConfig> _operationsByName = new TreeMap<>();
    private String _serviceId;
    private String _serviceType;

    public ConnectionConfig(
            String serviceId,
            String serviceType,
            String connectionId,
            String host,
            String virtualHost,
            String userName,
            String password
    ) {
        _serviceId = serviceId;
        _serviceType = serviceType;
        _connectionId = connectionId;
        _host = host;
        _virtualHost = virtualHost;
        _userName = userName;
        _password = password;

    }

    public String getServiceId() {
        return _serviceId;
    }

    public String getServiceType() {
        return _serviceType;
    }

    public String getConnectionId() {
        return _connectionId;
    }

    public String getHost() {
        return _host;
    }

    public String getVirtualHost() {
        return _virtualHost;
    }

    public String getUserName() {
        return _userName;
    }

    public String getPassword() {
        return _password;
    }

    public Collection<OperationConfig> getOperations() {
        return _operationsByName.values();
    }

    public OperationConfig getOperationConfig(String operationName) {
        return _operationsByName.get(operationName);
    }

    public boolean hasOperation(String operationName) {
        return _operationsByName.containsKey(operationName);
    }

    public OperationConfig getOrAddOperation(
            OperationOwnership operationOwnership,
            OperationType operationType,
            String operationName
    ) {
        OperationConfig operationConfig = new OperationConfig(
                operationOwnership,
                operationType,
                operationName,
                getServiceType(),
                getServiceId(),
                false,
                false
        );
        getOrAddOperation(operationConfig);
        return operationConfig;
    }

    public OperationConfig getOrAddOperation(
            OperationOwnership operationOwnership,
            OperationType operationType,
            String operationName,
            boolean requiresAuthentication
    ) {
        OperationConfig operationConfig = new OperationConfig(
                operationOwnership,
                operationType,
                operationName,
                getServiceType(),
                getServiceId(),
                requiresAuthentication,
                false
        );
        getOrAddOperation(operationConfig);
        return operationConfig;
    }

    public OperationConfig getOrAddOperation(
            OperationOwnership operationOwnership,
            OperationType operationType,
            String operationName,
            boolean requiresAuthentication,
            boolean isAvailable
    ) {
        OperationConfig operationConfig = new OperationConfig(
                operationOwnership,
                operationType,
                operationName,
                getServiceType(),
                getServiceId(),
                requiresAuthentication,
                isAvailable
        );
        getOrAddOperation(operationConfig);
        return operationConfig;
    }

    private void getOrAddOperation(OperationConfig operationConfig) {
        _operationsByName.put(operationConfig.getOperationName(), operationConfig);
        _operationsByName.put(
                OperationNames.Heartbeat,
                new OperationConfig(
                        OperationOwnership.OwnedByThisService,
                        OperationType.Stream,
                        OperationNames.Heartbeat,
                        getServiceType(),
                        getServiceId()
                )
        );
    }

    @Override
    public String toString() {
        return "MessagingConfig{" +
                "_connectionId='" + _connectionId + '\'' +
                ", _host='" + _host + '\'' +
                ", _virtualHost='" + _virtualHost + '\'' +
                ", _userName='" + _userName + '\'' +
                ", _password='" + _password + '\'' +
                '}';
    }
}
