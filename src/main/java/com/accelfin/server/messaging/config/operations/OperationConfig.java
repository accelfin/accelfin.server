package com.accelfin.server.messaging.config.operations;

/**
 * Config for an operation that is local to this service, i.e. it owns the operation and will fulfill requests for it
 */
public class OperationConfig {

    private String _serviceType;
    private String _serviceId;
    private OperationType _operationType;
    private String _operationName;
    private OperationOwnership _operationOwnership;
    private boolean _requiresAuthentication;
    private boolean _isAvailable;

    public OperationConfig(
            OperationOwnership operationOwnership,
            OperationType operationType,
            String operationName,
            String serviceType,
            String serviceId
    ) {
        this(operationOwnership, operationType, operationName, serviceType, serviceId, false, false);
    }

    public OperationConfig(
            OperationOwnership operationOwnership,
            OperationType operationType,
            String operationName,
            String serviceType,
            String serviceId,
            boolean requiresAuthentication
    ) {
        this(operationOwnership, operationType, operationName, serviceType, serviceId, requiresAuthentication, false);
    }

    public OperationConfig(
            OperationOwnership operationOwnership,
            OperationType operationType,
            String operationName,
            String serviceType,
            String serviceId,
            boolean requiresAuthentication,
            boolean isAvailable
    ) {
        _operationOwnership = operationOwnership;
        _operationType = operationType;
        _operationName = operationName;
        _serviceType = serviceType;
        _serviceId = serviceId;
        _requiresAuthentication = requiresAuthentication;
        _isAvailable = isAvailable;
    }

    public OperationOwnership getOperationOwnership() {
        return _operationOwnership;
    }

    public String getOperationName() {
        return _operationName;
    }

    public OperationType getOperationType() {
        return _operationType;
    }

    public boolean getRequiresAuthentication() {
        return _requiresAuthentication;
    }

    public String getServiceType() {
        return _serviceType;
    }

    public String getServiceId() {
        return _serviceId;
    }

    public boolean getIsAvailable() {
        return _isAvailable;
    }

    public void setIsAvailable(boolean available) {
        _isAvailable = available;
    }
}