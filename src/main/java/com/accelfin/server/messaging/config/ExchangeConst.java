package com.accelfin.server.messaging.config;

public class ExchangeConst {
    public static final String REQUEST_EXCHANGE_NAME = "request.exchange";
    public static final String RESPONSE_EXCHANGE_NAME = "response.exchange";
}
