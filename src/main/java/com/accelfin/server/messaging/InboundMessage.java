package com.accelfin.server.messaging;

import com.accelfin.server.AuthenticationStatus;
import com.accelfin.server.SessionToken;
import com.accelfin.server.messaging.operations.CompletionReason;
import com.google.protobuf.Message;

import java.time.Instant;

/**
 * note not all props on this type will be used depending upon what you're doing.
 * It's one of those things where a flat type is better than an abstraction for each specific type of inbound message
 */
public final class InboundMessage<TPayloadDto extends Message> {

    private String _correlationId;
    private String _operationName;
    private SessionToken _sessionToken;
    private AuthenticationStatus _authenticationStatus;
    private String _replyTo;
    private boolean _isCompleted;
    private CompletionReason _completionReason;
    private Instant _timestamp;
    private TPayloadDto _payload;

    public InboundMessage(
            String correlationId,
            String operationName,
            SessionToken sessionToken,
            AuthenticationStatus authenticationStatus,
            String replyTo,
            boolean isCompleted,
            CompletionReason completionReason,
            Instant timestamp,
            TPayloadDto payload
    ) {
        _correlationId = correlationId;
        _operationName = operationName;
        _sessionToken = sessionToken;
        _authenticationStatus = authenticationStatus;
        _replyTo = replyTo;
        _isCompleted = isCompleted;
        _completionReason = completionReason;
        _timestamp = timestamp;
        _payload = payload;
    }

    public TPayloadDto getPayload() {
        return _payload;
    }

    public boolean isCompleted() {
        return _isCompleted;
    }

    public CompletionReason getCompletionReason() {
        return _completionReason;
    }

    public SessionToken getSessionToken() {
        return _sessionToken;
    }

    public String getConnectionId() {
        return _sessionToken.getConnectionId();
    }

    public String getOperationName() {
        return _operationName;
    }

    public String getCorrelationId() {
        return _correlationId;
    }

    public String getReplyTo() {
        return _replyTo;
    }

    public Instant getTimestamp() {
        return _timestamp;
    }

    public AuthenticationStatus getAuthenticationStatus() {
        return _authenticationStatus;
    }
}
