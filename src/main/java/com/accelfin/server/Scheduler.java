package com.accelfin.server;

import java.time.Instant;

public interface Scheduler {
    void schedule(Runnable runnable, long delayMilliseconds);
    Instant now();
}
