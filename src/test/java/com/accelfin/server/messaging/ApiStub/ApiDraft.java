package com.accelfin.server.messaging.ApiStub;

import com.accelfin.server.AuthenticationStatus;
import com.accelfin.server.TestBase;
import com.accelfin.server.messaging.*;
import com.accelfin.server.messaging.InboundMessage;
import com.accelfin.server.messaging.operations.*;
import com.google.protobuf.*;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

class GetPrivateStuffRequest extends FakeMessage {}
class GetPrivateStuffResponse extends FakeMessage {}
class PriceUpdateResponse extends FakeMessage {}
class PriceRequest extends FakeMessage {}
class PriceResponse extends FakeMessage {}
class StubLogonRequest extends FakeMessage {}
class StubLogonResponse extends FakeMessage {}


@Ignore
public class ApiDraft extends TestBase{
    private Connection _connection1;

//    @Before
//    public void setUp() {
//        TestSystem = new TestSystem()
//                .addConnection("connection1",
//                        cb -> {
//                            // cb.withOperation()
//                        })
//                .createModel();
//        _connection1 = TestSystem.Model.getConnections().getConnection("connection1");
//    }

    @Test
    public void serverApiDraft() {
        // does away with manual operation configuration
        // who owns operation status? -> an entity on the model

        /////////////////////////////////////////////////////////////////////////////////////////
        // SERVER SIDE APIS
        /////////////////////////////////////////////////////////////////////////////////////////

        // ** Request getStreamOperation ** //
        // For locally owned request -> n responses,
        ServerRequestStreamOperation<StubLogonRequest, StubLogonResponse> logonOperation = _connection1.exposeRequestStreamOperation(
                // OperationOwnership.OwnedByThisService, implied
                "logon",
                AuthenticationStatus.Unauthenticated
        );
        logonOperation.requests().observe(
                // calling this message as sometimes it's an ack to kill the getStreamOperation
                // Maybe InboundMessage should be called ClientMessageNotification as sometimes it
                // is a result of the server detecting a timeout, thus no client message was actually sent
                (InboundMessage<StubLogonRequest> message) -> {
                    if (message.isCompleted()) {
                        if (message.getCompletionReason() == CompletionReason.HEARTBEAT_FAILURE) {
                            // server killed the getStreamOperation due to heartbeat timeout
                        } else {
                            // client has killed the getStreamOperation explicitly,
                            // you'd clean up any stored subscriptions here ,
                            // in this instance payload would be null
                        }
                    } else {
                        // we create the response using the operation and the incomng message.
                        // this will wire up all the correct endpoint/operation information for the message
                        // Maybe OutboundMessage should be ServerMessageNotification?
                        logonOperation.sendResponse(message, new StubLogonResponse(), true, "Session2");
                    }
                });
        logonOperation.updateAvailability(true);

        // ** RPC ** //
        // RPC will have the same API as request getStreamOperation, however internally it'll
        // wire up the queues differently as it's effectively a private request->getStreamOperation operation.
        // internally requests will come on a special queue (from memory) and responses go out via the senders 'reply to' header.
        ServerRequestRPCStreamOperation<GetPrivateStuffRequest, GetPrivateStuffResponse> getPrivateStuffOperation = _connection1.exposeRPCOperation(
                // OperationOwnership.OwnedByThisService, implied
                "getPrivateStuff", //
                AuthenticationStatus.Authenticated
        );
        getPrivateStuffOperation.requests().observe((InboundMessage<GetPrivateStuffRequest> message) -> {
            getPrivateStuffOperation.sendRpcResponse(message, new GetPrivateStuffResponse(), true);
        });
        // use getPrivateStuffOperation as you do a normal request-> getStreamOperation

        // ** Stream ** //
        // for getStreamOperation operations for no-request->response streams
        ServerStreamOperation<PriceUpdateResponse> getPricesOperation = _connection1.exposeStreamOperation(
                "getPrices"
        );
        getPricesOperation.updateAvailability(true);
        //.. dispatch example:
        getPricesOperation.sendResponse(new PriceUpdateResponse(), "sessionToken1", "sessionToken1");
    }

    @Test
    public void clientApiDraft() {

        /////////////////////////////////////////////////////////////////////////////////////////
        // CLIENT SIDE APIS
        // Should have a similar API to the JS client
        /////////////////////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        // ** getRequestStreamOperation **
        // getRequestStreamOperation: internally depending on the operation configuration of the server,
        // this may be an RPC call or a call to a direct exchange
        ClientRequestStreamOperation<StubLogonRequest, StubLogonResponse> logonOperation = _connection1.getRequestStreamOperation(
                "ServiceType",
                "OperationName",
                true
        );
        logonOperation
                .sendRequest( new StubLogonRequest())
                .observe((InboundMessage<StubLogonResponse> message) -> {
                    if (message.isCompleted()) {
                        if (message.getCompletionReason() == CompletionReason.HEARTBEAT_FAILURE) {

                        } else if (message.getCompletionReason() == CompletionReason.SERVER_CLOSED) {

                        }
                    }
                }
            );

        ////////////////////////////////////////////////////////////////////////
        // ** getStreamOperation **
        ClientStreamOperation<PriceResponse> priceOperation = _connection1.getStreamOperation(
                "ServiceType",
                "OperationName",
                true
        );
        priceOperation
                .responses()
                .observe((InboundMessage<PriceResponse> message) -> {
                            if (message.isCompleted()) {
                                if (message.getCompletionReason() == CompletionReason.HEARTBEAT_FAILURE) {

                                } else if (message.getCompletionReason() == CompletionReason.SERVER_CLOSED) {

                                }
                            }
                        }
                );
    }
}

class FakeMessage implements Message {
    @Override
    public void writeTo(CodedOutputStream output) throws IOException {

    }

    @Override
    public int getSerializedSize() {
        return 0;
    }

    @Override
    public Parser<? extends Message> getParserForType() {
        return null;
    }

    @Override
    public ByteString toByteString() {
        return null;
    }

    @Override
    public byte[] toByteArray() {
        return new byte[0];
    }

    @Override
    public void writeTo(OutputStream output) throws IOException {

    }

    @Override
    public void writeDelimitedTo(OutputStream output) throws IOException {

    }

    @Override
    public Builder newBuilderForType() {
        return null;
    }

    @Override
    public Builder toBuilder() {
        return null;
    }

    @Override
    public Message getDefaultInstanceForType() {
        return null;
    }

    @Override
    public boolean isInitialized() {
        return false;
    }

    @Override
    public List<String> findInitializationErrors() {
        return null;
    }

    @Override
    public String getInitializationErrorString() {
        return null;
    }

    @Override
    public Descriptors.Descriptor getDescriptorForType() {
        return null;
    }

    @Override
    public Map<Descriptors.FieldDescriptor, Object> getAllFields() {
        return null;
    }

    @Override
    public boolean hasOneof(Descriptors.OneofDescriptor oneof) {
        return false;
    }

    @Override
    public Descriptors.FieldDescriptor getOneofFieldDescriptor(Descriptors.OneofDescriptor oneof) {
        return null;
    }

    @Override
    public boolean hasField(Descriptors.FieldDescriptor field) {
        return false;
    }

    @Override
    public Object getField(Descriptors.FieldDescriptor field) {
        return null;
    }

    @Override
    public int getRepeatedFieldCount(Descriptors.FieldDescriptor field) {
        return 0;
    }

    @Override
    public Object getRepeatedField(Descriptors.FieldDescriptor field, int index) {
        return null;
    }

    @Override
    public UnknownFieldSet getUnknownFields() {
        return null;
    }
}