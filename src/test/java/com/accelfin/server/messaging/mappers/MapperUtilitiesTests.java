package com.accelfin.server.messaging.mappers;

import com.accelfin.server.messaging.dtos.DecimalDto;
import org.junit.Test;

import java.math.BigDecimal;

import static com.accelfin.server.messaging.mappers.SimpleTypesMapper.mapBigDecimalFromDto;
import static com.accelfin.server.messaging.mappers.SimpleTypesMapper.mapBigDecimalToDto;
import static org.junit.Assert.assertEquals;

public class MapperUtilitiesTests {
    @Test
    public void mapBigDecimalToDtoMapsCorrectly() {
        BigDecimal d = BigDecimal.valueOf(1.12345d);
        DecimalDto dto = mapBigDecimalToDto(d);
        BigDecimal d2 = mapBigDecimalFromDto(dto);
        assertEquals(d, d2);
    }
}