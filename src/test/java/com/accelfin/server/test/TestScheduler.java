package com.accelfin.server.test;

import com.accelfin.server.Scheduler;

import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

public class TestScheduler implements Scheduler {
    NavigableMap<Long, ArrayList<Runnable>> _runnablesByExpirationMs = new TreeMap<>();
    private long _nowMs = 0;

    @Override
    public void schedule(Runnable runnable, long delayMilliseconds) {
        long expirationMs = _nowMs + delayMilliseconds;
        ArrayList<Runnable> runnables = _runnablesByExpirationMs.get(expirationMs);
        if(runnables == null) {
            runnables = new ArrayList<>();
            _runnablesByExpirationMs.put(expirationMs, runnables);
        }
        runnables.add(runnable);
    }

    public long nowMs() {
        return _nowMs;
    }

    @Override
    public Instant now() {
        return Instant.ofEpochMilli(nowMs());
    }

    public void advanceTimeBy(long delay) {
        _nowMs+=delay;
        SortedMap<Long, ArrayList<Runnable>> expiredItemsMap = _runnablesByExpirationMs.headMap(_nowMs, true);
        List<Runnable> expiredItems = expiredItemsMap
                .values()
                .stream()
                .flatMap(List::stream)
                .collect(Collectors.toList());
        expiredItemsMap.clear();
        for(Runnable runnable : expiredItems) {
            runnable.run();
        }
    }
}
